#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "mpi.h"

#define N 1000

#define MASTER_TAG 1515
#define SLAVE_TAG 1789

void master(char *proc_name, int rank, int world_size) {
    MPI_Status status;
    double data[N], start_time, end_time, *part_data;
    int dataSize = N - 1, dataset_valid = 1, i, iProc, offset, doubles_number;

    start_time = MPI_Wtime();
    printf("[%s] Master started (tag=%d, rank=%d)\n", proc_name, MASTER_TAG, rank);

    // Initialize data
    for (i = 0; i < dataSize; i++) data[i] = i;

    // Compute number of slave and doubles by slave
    int count_slave = world_size - 1;
    int per_slave = (dataSize / (count_slave));
    int last_slave = (dataSize / (count_slave)) + (dataSize % (count_slave));

    printf("[%s] Divide array in %d parts of %d elements (last: %d)\n", proc_name, count_slave, per_slave, last_slave);

    part_data = malloc(sizeof(double) * per_slave);

    // Divide data and sent it to slave
    for (iProc = 1; iProc <= count_slave; iProc++) {

        // If this isn't the last message
        if (iProc < count_slave) {
            offset = (iProc - 1) * per_slave;
            doubles_number = per_slave;
        } else { // Last slave can have more double than the other (dataSize%count_slave != 0)
            offset = (iProc - 1) * per_slave;
            free(part_data);
            part_data = malloc(sizeof(double) * last_slave);
            doubles_number = last_slave;
        }

        // Copy the data that will be sent to the slave iProc
        memcpy(part_data, data + offset, doubles_number * sizeof(double));

        // Effectively send data
        MPI_Send(part_data, doubles_number, MPI_DOUBLE_PRECISION, iProc, MASTER_TAG, MPI_COMM_WORLD);
        printf("[%s] Sent %d doubles to %d\n", proc_name, dataSize, iProc);
    }

    printf("[%s] Aggregate slave results...\n", proc_name);
    for (iProc = 1; iProc <= world_size - 1; iProc++) {
        offset = (iProc - 1) * per_slave; //Place in the array depends on the slave id
        MPI_Recv(data + offset, dataSize, MPI_DOUBLE_PRECISION, iProc, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        sleep(1);
    }

    end_time = MPI_Wtime();
    printf("Time spent: %fs. Leaving...\n", end_time - start_time);

    // Verify data
    for (i = 0; i < dataSize; i++) {
        if (data[i] != (double) (i + 1)) {
            printf("Verify failed, %f != %d\n", data[i], i + 1);
            dataset_valid = 0; //Mark dataset as not valid
        }
    }
    if (dataset_valid) printf("Dataset valid !\n");
}

void slave(char *proc_name, int rank) {
    int number_doubles, i;
    double *data;
    MPI_Status status;

    printf("[%s] Slave started with rank %d\n", proc_name, rank);

    // Probe for an incoming message from master
    MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

    // Get the size of the message (number of doubles passed)
    MPI_Get_count(&status, MPI_DOUBLE_PRECISION, &number_doubles);

    data = (double *) malloc(sizeof(double) * number_doubles);

    // Received data from master
    MPI_Recv(data, number_doubles, MPI_DOUBLE_PRECISION, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    printf("[%s] Dynamically received %d doubles from 0\n", proc_name, number_doubles);

    // Process data
    printf("[%s] Processing data...", proc_name);
    for (i = 0; i < number_doubles; i++) data[i] += 1;
    printf("  finished.\n");

    // Send back the processed data to master
    MPI_Send(data, number_doubles, MPI_DOUBLE_PRECISION, 0, SLAVE_TAG, MPI_COMM_WORLD);

    free(data);
}

int main(int argc, char *argv[]) {
    int rank, size, name_len;
    char proc_name[MPI_MAX_PROCESSOR_NAME];

    MPI_Init(&argc, (char ***) &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Get_processor_name(proc_name, &name_len);

    if (rank == 0) {
        master(proc_name, rank, size);
    } else {
        slave(proc_name, rank);
    }

    MPI_Finalize();
    return EXIT_SUCCESS;
}

