#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  // Start parallel code
  MPI_Init(&argc, (char ***) &argv);

  int rank, name_len, world_size;
  char proc_name[MPI_MAX_PROCESSOR_NAME];

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Get_processor_name(proc_name, &name_len);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  if(rank == 0) printf("World size = %d", world_size);

  printf("Hello World by %s, proc %d\n", proc_name, rank);

  MPI_Finalize();
  // End parallel

  return EXIT_SUCCESS;
}
