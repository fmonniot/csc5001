#include <stdlib.h>
#include <stdio.h>

int main() {

    int a = 10, b = 20, c = 30;

#pragma omp parallel private (a)
    {
        printf("%d\n", a++);
    }
#pragma omp parallel firstprivate (b)
    {
        printf("%d\n", b++);
    }
#pragma omp parallel shared (c)
    {
        printf("%d\n", c++);
    }

    int res = 1;
#pragma omp parallel reduction(+:res)
    {
        res = 2;
    }
    printf("%d", res);

    return EXIT_SUCCESS;
}