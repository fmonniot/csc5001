#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char *argv[]) {

    int max_threads = -1;
#ifdef _OPENMP
    max_threads = omp_get_max_threads();
#endif

    printf("Ready to use %d threads. Force that to 4.\n", max_threads);

#ifdef _OPENMP
    omp_set_num_threads(4);
#endif

#pragma omp parallel
    {
        int current_thread = -1;
#ifdef _OPENMP
        current_thread = omp_get_thread_num();
#endif
        printf("Hello Wold from thread n°%d\n", current_thread);
    }

    return EXIT_SUCCESS;
}