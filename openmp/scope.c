#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char *argv[]) {
    printf("Ready to use %d threads.\n", omp_get_max_threads());

    int first = 10, second = 20, third = 30;

#pragma omp parallel private(first) firstprivate(second) shared(third)
    {
        int current_thread = omp_get_thread_num();
        first += current_thread;
        second += current_thread;
//        third += current_thread;

        printf("[T-%d] first=%d, second=%d, third=%d\n", current_thread, first, second, third);
    }

    printf("After parallel section: first=%d, second=%d, third=%d\n", first, second, third);
    return EXIT_SUCCESS;
}