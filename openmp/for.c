#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define ARRAY_SIZE 1000000
//#define ARRAY_SIZE 18
#define SHOW_READABLE(txt) (ARRAY_SIZE < 20) ? txt : ""

int main(int argc, char *argv[]) {
    printf("Ready to use %d threads.\n", omp_get_max_threads());

    int dst[ARRAY_SIZE], orig[ARRAY_SIZE];
    int i, cst = 42, tid;
    double start, end;

    // Fill orig
    for(i=0; i < ARRAY_SIZE; i++) orig[i] = 0;

    omp_sched_t scheduler;
#pragma omp parallel private(i, tid) shared(orig, dst, cst)
    {
        tid = omp_get_thread_num();
        int modifier = 0;

#pragma omp for schedule(static, ARRAY_SIZE/2) nowait
        for (i = 0; i < ARRAY_SIZE; i++) {
            omp_get_schedule(&scheduler, &modifier);
            printf(SHOW_READABLE("[tid=%d, i=%d]\tscheduler=%d, modifier=%d\n"), tid, i, scheduler, modifier);
            dst[i] = orig[i] + cst;
        }

        printf(SHOW_READABLE("[tid=%d] Called after for loop.\n"), tid);
    }

    // Show dst if not too large
    printf(SHOW_READABLE("\ndst=["));
    for(i=0; i < (ARRAY_SIZE - 1); i++) printf(SHOW_READABLE("%d,"), dst[i]);
    printf(SHOW_READABLE("%d]\n"), dst[i]);

    // Compute sum of dst array

    long dst_sum;

    start = omp_get_wtime();
    dst_sum = 0;
    for(i=0; i < ARRAY_SIZE; i++) dst_sum += dst[i];
    end = omp_get_wtime();
    printf("dst_sum=%ld. Took %fms.\n", dst_sum, (end - start)*1000);

    start = omp_get_wtime();
    dst_sum = 0;
#pragma omp parallel private(i) shared(dst) reduction(+:dst_sum)
    {
#pragma omp for schedule(dynamic, 1000)
        for (i = 0; i < ARRAY_SIZE; i++) dst_sum += dst[i];
    }

    end = omp_get_wtime();
    printf("dst_sum=%ld. Took %fms.\n", dst_sum, (end - start)*1000);

    return EXIT_SUCCESS;
}